package envsensor

import (
	"bytes"
	"encoding/binary"
	"errors"

	"github.com/tarm/serial"
)

type SensorDevice struct {
	filename string
	port     *serial.Port
}

type LatestSensingData struct {
	Sequence    uint8
	SensingData
}

func (s *LatestSensingData) setSensingDataResponse(response *ReadResponseFormat) error {
	s.temperature = float64(response.temperature) / 100
	s.humidity = float64(response.humidity) / 100
	s.light = response.light
	s.pressure = float64(response.pressure) / 1000
	s.noise = float64(response.noise) / 100
	s.etvoc = response.etvoc
	s.eco2 = response.eco2
	return nil
}

func (s *LatestSensingData) Temperature() float64 {
	return s.temperature
}

func (s *LatestSensingData) Humidity() float64 {
	return s.humidity
}

func (s *LatestSensingData) Light() int16 {
	return s.light
}

func (s *LatestSensingData) Pressure() float64 {
	return s.pressure
}

func (s *LatestSensingData) Noise() float64 {
	return s.noise
}

func (s *LatestSensingData) Etvoc() int16 {
	return s.etvoc
}

func (s *LatestSensingData) Eco2() int16 {
	return s.eco2
}

type SensingData struct {
	temperature float64
	humidity    float64
	light       int16
	pressure    float64
	noise       float64
	etvoc       int16
	eco2        int16
}

type CommonFrame struct {
	header  uint16
	length  uint16
	crc16   uint16
}

type PayloadFrame struct {
	command uint8
	address uint16
	code    uint8
}

type ReadResponseFormat struct {
	number      uint8
	temperature int16
	humidity    int16
	light       int16
	pressure    int32
	noise       int16
	etvoc       int16
	eco2        int16

	discomfort int16
	heat       int16
	vibration  uint8
	si         uint16
	pga        uint16
	intensity  uint16
}

func Open(name string) (*SensorDevice, error) {
	c := &serial.Config{Name: name, Baud: 115200}
	p, err := serial.OpenPort(c)
	if err != nil {
		return nil, err
	}
	device := &SensorDevice{port: p, filename: name}
	return device, nil
}

func (s *SensorDevice) Close() (error) {
	if port := s.port; port == nil {
		return errors.New("Cannot close the port which is already closed")
	} else {
		s.port = nil
		return port.Close()
	}
}

func (s *SensorDevice) GetLatestSensingData() (LatestSensingData, error) {
	data := LatestSensingData{}
	response, err := s.readCommand(0x5021)
	if err != nil {
		return data, err
	}

	data.Sequence = response.number
	data.setSensingDataResponse(response)

	return data, nil
}

func (s *SensorDevice) readCommand(address uint16) (*ReadResponseFormat, error) {
	command, err := s.command(address)
	if err != nil {
		return nil, err
	}

	frameBuf, err := s.sendCommand(command)
	if err != nil {
		return nil, err
	}

	frameReader := bytes.NewReader(frameBuf)
	frame := CommonFrame{}
	// Read header
	binary.Read(frameReader, binary.LittleEndian, &frame.header)
	if frame.header != 0x4252 { // 0x4252 is "BR"
		return nil, errors.New("header's magic number is incorrect")
	}
	// Read payload's length (this length includes CRC value)
	binary.Read(frameReader, binary.LittleEndian, &frame.length)

	// Read payload
	payloadBuf := make([]byte, frame.length - 2)
	if n, err := frameReader.Read(payloadBuf); err != nil || n != len(payloadBuf) {
		return nil, errors.New("payload read error")
	}

	// Read CRC16
	binary.Read(frameReader, binary.LittleEndian, &frame.crc16)
	// [ToDO] check whether payload's crc is corrent
	// if s.crc16(payload) == frame.crc16

	payloadReader := bytes.NewReader(payloadBuf)
	payload := PayloadFrame{}

	// Read command
	binary.Read(payloadReader, binary.LittleEndian, &payload.command)

	// Read address
	binary.Read(payloadReader, binary.LittleEndian, &payload.address)
	if payload.address != address {
		return nil, errors.New("mismatch address")
	}

	// Read Data
	return s.parseResponse(address, payloadBuf[3:])
}

func (s *SensorDevice) command(address uint16) ([]byte, error) {
	switch address {
	case 0x5021:
		return []byte{0x52, 0x42, 0x05, 0x00, 0x01, 0x21, 0x50, 0xe2, 0x4b}, nil
	}
	return nil, errors.New("not implemented address")
}

func (s *SensorDevice) parseResponse(address uint16, buf []byte) (*ReadResponseFormat, error) {
	switch address {
	case 0x5021:
		return s.parseSensorLatestData(buf)
	}
	return nil, errors.New("not implemented address")
}

func (s *SensorDevice) parseSensorLatestData(buf []byte) (*ReadResponseFormat, error) {
	response := ReadResponseFormat{}
	reader := bytes.NewReader(buf)

	binary.Read(reader, binary.LittleEndian, &response.number)
	binary.Read(reader, binary.LittleEndian, &response.temperature)
	binary.Read(reader, binary.LittleEndian, &response.humidity)
	binary.Read(reader, binary.LittleEndian, &response.light)
	binary.Read(reader, binary.LittleEndian, &response.pressure)
	binary.Read(reader, binary.LittleEndian, &response.noise)
	binary.Read(reader, binary.LittleEndian, &response.etvoc)
	binary.Read(reader, binary.LittleEndian, &response.eco2)
	return &response, nil
}

func (s *SensorDevice) sendCommand(command []byte) ([]byte, error) {
	if s.port == nil {
		return nil, errors.New("Cannot read/write the port which is already closed")
	}
	_, err := s.port.Write(command)
	if err != nil {
		return nil, err
	}
	buf := make([]byte, 128)
	_, err = s.port.Read(buf)
	if err != nil {
		return nil, err
	}
	return buf, nil
}
