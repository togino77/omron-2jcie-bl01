module gitlab.com/togino77/omron-2jcie-bl01

go 1.14

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
)
